# Build and Run a NodeJs container using Vagrant, Podman, Buildah and Skopeo

## Prereqs

### Create a simple NodeJS on the host machine

Note that these step has been done already. A simple NodeJs app exists in the project directory and can be run locally.

```shell
npm init

npm install express --save
```

Sample application:

```javascript
const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
    res.send('Hello world of buildah and podman!')
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
```

To test locally run following command and browse <http://localhost:3000>.

```shell
node index.js
```

## Vagrantfile

- centos/8
- bridge network (find ip in the vm)
- install podman, buildah and skopeo
- /vagrant directory contains:
  - a simple NodeJS app
  - a Dockerfile to build an image that runs the app

Provisioning this VM would create a playground for Podman, Buildah and Skopeo.

```shell
vagrant up

vagrant ssh 
```

## Buildah

For this exercise buildah is used to build an image from Dockerfile.
Note: Download the node image from docker.io

```shell
cd /vagrant

buildah bud -t mynodejsimg
```

### Some other commands for buildah

```shell
# help
buildah help

# show images
buildah images

# show containers
buildah containers

# build an image from a Dockerfile
buildah bud

# create a container using python:alpine as base image 
#and name it as "myPythonContainer"
buildah from --pull --name "myPythonContainer" python:alpine

# run a directory in the previously created container
buildah run myPythonContainer mkdir -p /usr/src/mypythonapp

# copy files into the container filesystem
buildah copy  myPythonContainer ./helloworld.py /usr/src/mypythonapp/

# set working directory in the previously created container
buildah config --workingdir /usr/src/mypythonapp myPythonContainer

# set entrypoint in the previously created container
buildah config --entrypoint '["pyhton", "helloworld.py"]' myPythonContainer

# login to the container shell (or use /bin/bash if bash shell
#exists)
buildah run myPythonContainer sh

# create an image from the container (with only one layer using
#squash arguement)
buildah commit --squash myPythonContainer my-python-image

# remove the working container
buildah rm myPythonContainer
```

## Podman

```shell
# show images 
podman images

# run and test the container 
podman run -it -d -p 8080:3000 --name mynodejsapp mynodejsimg

podman logs mynodejsapp
# must see "Example app listening at http://localhost:3000"

curl http://localhost:8080
# must see "Hello World of buildah and podman!"

# login and push the image to the dockerhub (registery 
#configuration at /etc/containers/registries.conf)
podman login docker.io

podman push localhost/mynodejsimg flyingfishipek/mynodejsapp
```

## Skopeo

We will use Skopeo to inspect image layers and copy the image from Dockerhub to Gitlab's project registry.

```shell
# inspect a remote image (since our repository is public login 
#is not required to inspect)
skopeo inspect docker://docker.io/flyingfishipek/mynodejsapp:playwithskopeo

# copy image from DockerHub to Gitlab's registry (assume 
#credentials are set using env vars): 
skopeo copy docker://docker.io/flyingfishipek/mynodejsapp:playwithskopeo --src-creds flyingfishipek:$SRCPASSWD docker://registry.gitlab.com/flyingfishipek/podman-buildah-skopeo-demo-project/mynodejsapp:playwithskopeo-in-gitlab --dest-creds flyingfishipek:$DESTPASSWD
```

Note that [this](https://www.systutorials.com/docs/linux/man/1-skopeo/) is a good manual for Skopeo commands.