FROM node:10

RUN mkdir -p /usr/src/mynodeapp

COPY /. /usr/src/mynodeapp     

WORKDIR /usr/src/mynodeapp     

RUN npm install

ENTRYPOINT ["node", "index.js"]